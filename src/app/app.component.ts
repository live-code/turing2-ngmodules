import { Component } from '@angular/core';
import { CounterService } from './core/services/counter.service';

@Component({
  selector: 'tur-root',
  template: `
    
    <tur-nav-bar></tur-nav-bar>
   
    <hr>
    <router-outlet></router-outlet>
  `,

})
export class AppComponent {
}
