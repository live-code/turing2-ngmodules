import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TabbarComponent } from './components/tabbar.component';
import { CardModule } from './components/card.module';

@NgModule({
  declarations: [
    // components, directive, pipes

    TabbarComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    CardModule,
  ],
  exports: [
    // components, directive, pipes, modules
    CardModule,
    TabbarComponent,
  ]
})
export class SharedModule { }
