import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tur-nav-bar',
  template: `
    <button routerLinkActive="active" routerLink="home">home</button>
    <button routerLinkActive="active" routerLink="login">login</button>
    <button routerLinkActive="active" routerLink="contacts">contacts</button>
    <button routerLinkActive="active" routerLink="catalog">catalog</button>

  `,
  styles: [`
    .active { background-color: orange}
  `]
})
export class NavBarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
