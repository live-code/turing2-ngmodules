import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tur-login',
  template: `
 
    <router-outlet></router-outlet>
    <hr>
    <button routerLinkActive="active" routerLink="sign">signin</button>
    <button routerLinkActive="active" routerLink="registration">regi</button>
    <button routerLinkActive="active" routerLink="lost">lost</button>
  `,
  styles: [`
    .active { background-color: cyan}
  `]
})
export class LoginComponent  {

}
