import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { SignInComponent } from './components/sign-in.component';
import { RegistrationComponent } from './components/registration.component';
import { LostPassComponent } from './components/lost-pass.component';

@NgModule({
  declarations: [
    LoginComponent,
    SignInComponent,
  //  RegistrationComponent,
    LostPassComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: LoginComponent,
        children: [
         // { path: 'registration', component: RegistrationComponent },
          { path: 'registration', loadChildren: () => import('./components/registration.module').then(m => m.RegistrationModule) },
          { path: 'lost', component: LostPassComponent },
          { path: 'sign', component: SignInComponent },
          { path: '', redirectTo: 'sign'}
        ]
      },

    ])
  ]
})
export class LoginModule { }
