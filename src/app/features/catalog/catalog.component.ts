import { Component } from '@angular/core';

@Component({
  selector: 'tur-catalog',
  template: `
    <tur-card></tur-card>
    <button routerLinkActive="active" routerLink="/catalog/guida">guida</button>
    <button routerLinkActive="active" routerLink="promo">promo</button>
    <button routerLinkActive="active" routerLink="../catalog/offerte">offerte</button>
    <hr>
    
    <router-outlet></router-outlet>
    
  `,
  styles: [`
    .active { background-color: purple; color: white }
  `]
})
export class CatalogComponent  {

}
