import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CatalogComponent } from './catalog.component';
import { CatalogService } from './services/catalog.service';
import { OfferteComponent } from './pages/offerte.component';
import { GuidaComponent } from './pages/guida.component';
import { PromoComponent } from './pages/promo.component';
import { CardComponent } from '../../shared/components/card.component';
import { SharedModule } from '../../shared/shared.module';
import { CardModule } from '../../shared/components/card.module';

@NgModule({
  declarations: [
    CatalogComponent,
    OfferteComponent,
    GuidaComponent,
    PromoComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    RouterModule.forChild([
      {
        path: '',
        component: CatalogComponent,
        children: [
          { path: 'guida', component: GuidaComponent },
          { path: 'promo', component: PromoComponent },
          { path: 'offerte', component: OfferteComponent },
          { path: '', redirectTo: 'guida' },
        ]
      }
    ])
  ],
  providers: [
    CatalogService
  ]
})
export class CatalogModule { }
