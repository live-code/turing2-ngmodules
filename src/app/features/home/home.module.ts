import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { NewsComponent } from './components/news.component';
import { CarouselComponent } from './components/carousel.component';
import { HeroComponent } from './components/hero.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LanguageService } from '../../core/services/language.service';
import { CounterService } from '../../core/services/counter.service';
import { CardComponent } from '../../shared/components/card.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    HomeComponent,
    NewsComponent,
    CarouselComponent,
    HeroComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: HomeComponent }
    ])
  ],
  providers: []
})
export class HomeModule {}

