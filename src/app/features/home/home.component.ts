import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LanguageService } from '../../core/services/language.service';
import { CounterService } from '../../core/services/counter.service';
import { CatalogService } from '../catalog/services/catalog.service';

@Component({
  selector: 'tur-home',
  template: `
    <tur-card></tur-card>
    <tur-tabbar></tur-tabbar>
    <h1 *ngIf="true" (click)="counterService.inc()">Home {{counterService.value}}</h1>
    <input type="text" ngModel>
    <tur-carousel></tur-carousel>
    <tur-news></tur-news>
    <tur-hero></tur-hero>
  `,
})
export class HomeComponent {

  constructor(public counterService: CounterService) {
    console.log(counterService.value)
  }


}
